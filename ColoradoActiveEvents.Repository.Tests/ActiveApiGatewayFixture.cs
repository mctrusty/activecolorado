﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using ColoradoActiveEvents.Model;
using System.Net.Http;
using NUnit.Framework;
using System.Net;

namespace ColoradoActiveEvents.Repository.Tests
{
    [TestFixture]
    public class ActiveApiGatewayFixture
    {
        [Test]
        [Category("ActualApiCall")]
        [Ignore("External API calls shouldn't be included in unit tests")]
        public void GetAll_returns_right_number()
        {
            IActiveApiGateway apiGateway = new ActiveApiGateway();
            var results = apiGateway.GetAll();
            var count = results.Count();
            Assert.AreEqual(100, count);
        }

        [Test]
        [Category("ActualApiCall")]
        [Ignore("External API calls shouldn't be included in unit tests")]
        public void GetAll_running_events_in_near_future()
        {
            var request = new ActiveApiRequest()
                {
                    Meta = "meta:startDate:daterange:today.. meta:channel=Running",
                    Number = "num=10"
                };
            IActiveApiGateway apiGateway = new ActiveApiGateway(request);
            var results = apiGateway.GetAll();

            Assert.AreEqual(10, results.Count());
        }

        [Test]
        [Category("ActualApiCall")]
        [Ignore("External API calls shouldn't be included in unit tests")]
        public void GetAll_returns_json()
        {
            var query = new ActiveApiRequest()
                {
                    Format = "v=json",
                    Number = "num=5"
                };
            IActiveApiGateway apiGateway = new ActiveApiGateway(query);
            var results = apiGateway.GetAll();
            Assert.AreEqual(5, results.Count());
        }

        [Test]
        [Category("ActualApiCall")]
        [Ignore("External API calls shouldn't be included in unit tests")]
        public void GetAll_xml_format_throws_error()
        {
            var query = new ActiveApiRequest()
            {
                Format = "v=xml",
                Number = "num=2"
            };
            IActiveApiGateway apiGateway = new ActiveApiGateway(query);
            var aggregate = Assert.Throws<AggregateException>(() => apiGateway.GetAll());
            Assert.IsInstanceOf<SerializationException>(aggregate.InnerExceptions.Single());
        }

        [Test]
        [Category("ActualApiCall")]
        [Ignore("External API calls shouldn't be included in unit tests")]
        public void WrongUrl_throws_exception()
        {
            var query = new ActiveApiRequest()
                {
                    ApiBase = new Uri("http://failingurltest/fail/"),
                };
            IActiveApiGateway apiGateway = new ActiveApiGateway(query);
            //var results = apiGateway.GetAll();
            var aggregate = Assert.Throws<AggregateException>(() => apiGateway.GetAll());
            Assert.IsInstanceOf<HttpRequestException>(aggregate.InnerExceptions.Single());
        }

    }
}

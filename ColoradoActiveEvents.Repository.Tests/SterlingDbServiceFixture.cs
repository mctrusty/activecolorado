﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ColoradoActiveEvents.Model;
using Moq;
using Moq.Language.Flow;
using NUnit.Framework;
using Wintellect.Sterling;
using Wintellect.Sterling.Database;
using Wintellect.Sterling.Server.FileSystem;

namespace ColoradoActiveEvents.Repository.Tests
{
    
    [TestFixture]
    public class SterlingDbServiceFixture : SterlingDbServiceFixtureBase
    {
        [Test]
        public void StartService()
        {
            var newDbInstance = new Mock<ISterlingDatabaseInstance>();
            newDbInstance.SetupGet(p => p.Name).Returns("TestDb");

            var newDbService = new Mock<ISterlingService>();
            newDbService.SetupGet(p => p.Database).Returns(newDbInstance.Object);

            //
            newDbInstance.Verify();
            Assert.AreEqual("TestDb", newDbService.Object.Database.Name);
            Assert.AreEqual("Test Database", DatabaseInstance.Name);
            
        }

        [Test]
        public void Select()
        {
            var recs = (from o in DatabaseInstance.Query<Activity, Guid>()
                        select o.LazyValue.Value);
            Assert.AreEqual(100, recs.Count());
        }

        [Test]
        public void Get()
        {
            int skip = 0;
            int take = 10;
            var recs = (from o in DatabaseInstance.Query<Activity, Guid>()
                        select o.LazyValue.Value);
            for (int i = 0; i < 10; i++)
            {
                skip = i * 10;
                Assert.AreEqual(10, recs.Skip(skip).Take(take).Count());
            }
        }

        [Test]
        public void GetByEventId()
        {
            string eventId = "1";

            var activity = (from o in DatabaseInstance.Query<Activity, Guid>()
                            select o.LazyValue.Value).FirstOrDefault(a => a.MetaInfo.EventId == eventId);

            Assert.AreEqual(eventId, activity.MetaInfo.EventId);
        }

        [Test]
        public void PopulateFromActiveApi()
        {
            var activeApi = new Mock<IActiveApiGateway>();
            var apiWorker = new Mock<IActivityCache>();

            var activity = (from o in DatabaseInstance.Query<Activity, Guid>()
                            select o.LazyValue.Value).Take(20);
            activeApi.Setup(p => p.GetAll()).Returns(activity);


            var activeRepository = new ActivityRepository(activeApi.Object, DatabaseInstance, apiWorker.Object);
            activeRepository.PopulateFromActiveApi(true);

            activeApi.Verify(g => g.GetAll(), Times.Exactly(1));

            var count = activeRepository.GetAllActivities().Count();
            //Assert.AreEqual(20, count);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ColoradoActiveEvents.Repository.Tests
{
    /// <summary>
    /// The main version uses a filesystem db, whereas much of the testing 
    /// relies on an in-memory version. Here we just want to check that the
    /// FileSystem db starts and stops as expected.
    /// </summary>
    [TestFixture]
    public class SterlingFileSystemDbFixture
    {
        
        [Test]
        public void StartFilesystemService()
        {
            const string dbNameExpected = "Colorado Active Events";
            var fsDBService = new SterlingFileSystemDBService();
            fsDBService.StartService();
            Assert.AreEqual(dbNameExpected, fsDBService.Database.Name);
            fsDBService.StopService();
        }
    }
}

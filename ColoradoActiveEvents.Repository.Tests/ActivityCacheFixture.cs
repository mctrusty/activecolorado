﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ColoradoActiveEvents.Model;
using Moq;
using NUnit.Framework;

namespace ColoradoActiveEvents.Repository.Tests
{
    [TestFixture]
    class ActivityCacheFixture
    {
        private Mock<IActiveApiGateway> _activeApiGateway ;
        private List<Activity> _tempActivities = new List<Activity>() { new Activity { EventId = "1" } };
        private ActivityCache _activityCache;
        
        [SetUp]
        public void SetUp()
        {
            _activeApiGateway = new Mock<IActiveApiGateway>();
            _activeApiGateway.Setup(g => g.GetAll()).Returns(_tempActivities);
            _activityCache = new ActivityCache(_activeApiGateway.Object);
        }

        [Test]
        public void ActivityCache_fills_on_load()
        {
            Thread.Sleep(1000); //Give cache a moment to set up
            _activeApiGateway.Verify(g => g.GetAll(), Times.AtLeastOnce());
        }

        [Test]
        public void ActivityCache_fills_list()
        {
            Thread.Sleep(1000); // give cache a moment to fill bc it's on a separate thread.
            var cached = _activityCache.GetCachedActivities();
            var cachedId = cached[0].EventId; 

            const string expectedId = "1";
            Assert.AreEqual(expectedId, cachedId);
        }

        [Test]
        [Category("Long Running")]
        [Ignore("Runs for > 1 minute")]
        public void ActivityCache_updates_after_one_minute()
        {
            Thread.Sleep(TimeSpan.FromSeconds(61));
            _activeApiGateway.Verify(g => g.GetAll(), Times.Exactly(2));
        }

        [Test]
        [Category("Long Running")]
        [Ignore("Runs for > 1 minute")]
        public void ActivityCache_updates_list()
        {
            _tempActivities = new List<Activity>() { new Activity { EventId = "2" } };
            _activeApiGateway.Setup(g => g.GetAll()).Returns(_tempActivities);
            
            Thread.Sleep(TimeSpan.FromSeconds(61));

            var cached = _activityCache.GetCachedActivities();
            var cachedId = cached[0].EventId; 
            const string expectedId = "2";
            Assert.AreEqual(expectedId, cachedId);
        }

        [TearDown]
        public void TearDown()
        {
            _activeApiGateway = null;
            _activityCache.Dispose(); 
            _activityCache = null;
        }
    }
}

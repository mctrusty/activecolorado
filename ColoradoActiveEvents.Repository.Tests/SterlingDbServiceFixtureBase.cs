﻿using System;
using System.Collections.Generic;
using ColoradoActiveEvents.Model;
using NUnit.Framework;
using Wintellect.Sterling;

namespace ColoradoActiveEvents.Repository.Tests
{
    public class SterlingDbServiceFixtureBase
    {
        protected SterlingEngine Engine;
        protected ISterlingDatabaseInstance DatabaseInstance;

        /// <summary>
        /// Setup code adapted from https://sites.google.com/site/sterlingdatabase/sterling-user-guide/getting-started
        /// </summary>
        [SetUp]
        public virtual void TestInit()
        {
            if (Engine == null)
            {
                Engine = new SterlingEngine();
                Engine.Activate();
                // create in-memory version (the default) for testing
                // alternately, we could create a filesystem db with ("\Test") for the location
                DatabaseInstance = Engine.SterlingDatabase.RegisterDatabase<SterlingTestDbInstance>();
                LoadDb();
            }
        }   
            

        public virtual void LoadDb()
        {
            var random = new Random();
            // test saving and reloading
            var list = new List<Activity>();
            for (var x = 0; x < 100; x++)
            {
                var testClass = new Activity
                    {
                        Summary = random.Next().ToString(),
                        Id = Guid.NewGuid(),
                        EscapedUrl = string.Format(@"http://{0}", random.Next().ToString()), 
                        MetaInfo = new Meta(){EventId = x.ToString()},
                    };

                list.Add(testClass);
                DatabaseInstance.Save(testClass);
            }
        }

        [TearDown]
        public virtual void TestCleanup()
        {
            if (Engine != null)
            {
                Engine.Dispose();
                Engine = null;
            }

            if (DatabaseInstance != null)
            {
                DatabaseInstance.Purge();
                DatabaseInstance = null;
            }

            GC.SuppressFinalize(this);
            GC.Collect();
        }
    }
}
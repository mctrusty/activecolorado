﻿using System;
using System.Collections.Generic;
using ColoradoActiveEvents.Model;
using Wintellect.Sterling.Database;

namespace ColoradoActiveEvents.Repository.Tests
{
    public class SterlingTestDbInstance : BaseDatabaseInstance
    {
        public override string Name
        {
            get { return "Test Database"; }
        }

        protected override List<ITableDefinition> RegisterTables()
        {
            return new List<ITableDefinition>
                {
                    CreateTableDefinition<Activity, Guid>(c => c.Id)
                };
        }
    }
}

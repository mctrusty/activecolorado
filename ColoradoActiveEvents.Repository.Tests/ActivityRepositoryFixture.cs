﻿using System;
using System.Linq;
using ColoradoActiveEvents.Model;
using NUnit.Framework;
using Moq;

namespace ColoradoActiveEvents.Repository.Tests
{
    [TestFixture]
    public class ActivityRepositoryFixture : SterlingDbServiceFixtureBase
    {
        [Test]
        public void CheckDbName()
        {
            Assert.AreEqual("Test Database", DatabaseInstance.Name);   
        }

        [Test]
        public void GetAllActivities_returns_all()
        {
            var api = new Mock<ActiveApiGateway>();
            var apiCache = new Mock<IActivityCache>();
            IActivityRepository repo = new ActivityRepository(api.Object, DatabaseInstance, apiCache.Object);
            var results = repo.GetAllActivities();
            Assert.AreEqual(100, results.Count());
        }

        [Test]
        public void Get_takes_right_amount()
        {
            var api = new Mock<ActiveApiGateway>();
            var apiCache = new Mock<IActivityCache>();
            IActivityRepository repo = new ActivityRepository(api.Object, DatabaseInstance, apiCache.Object);
            int take = 10;
            var results = repo.GetActivities(0, take);
            Assert.AreEqual(take, results.Count());
        }

        [Test]
        public void GetEvent_returns_event()
        {
            var api = new Mock<ActiveApiGateway>();
            var apiCache = new Mock<IActivityCache>();
            IActivityRepository repo = new ActivityRepository(api.Object, DatabaseInstance, apiCache.Object);
            var eventId = "1";
            var theEvent = repo.GetActivity(eventId);
            Assert.AreEqual(eventId, theEvent.MetaInfo.EventId);
        }
    }
}

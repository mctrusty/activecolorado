﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using ColoradoActiveEvents.Model;

namespace ColoradoActiveEvents.Repository
{
    /// <summary>
    /// Calls the Active.com API to get a list of activities
    /// </summary>
    public class ActiveApiGateway : IActiveApiGateway
    {
        
        #region private members

        //private static string _queryString;
        private static ActiveApiRequest _request;

        #endregion
        #region private methods
        private static Activities GetActivitiesFromActive()
        {
            var client = new HttpClient {BaseAddress = _request.ApiBase};
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                var response = client.GetAsync(_request.QueryString).Result;
                response.EnsureSuccessStatusCode();
                var activities = response.Content.ReadAsAsync<Activities>().Result;
                return activities;
            }
            catch (HttpRequestException e)
            {
                Trace.WriteLine("HttpRequest Error: " + e.Message);
                throw;
            }
            catch (Newtonsoft.Json.JsonException jEx)
            {
                Trace.WriteLine("Json Error: " + jEx.Message);
                throw;
            }
            catch (AggregateException aex)
            {
                aex.Flatten().Handle(ex =>
                    {
                        /*if (ex is HttpRequestException)
                        {
                            Trace.Write(ex.Message);
                        }
                        if (ex is SerializationException)
                        {
                            Trace.WriteLine(ex.Message);
                        }*/
                        Trace.WriteLine(ex.Message);
                        return true; // handled
                    });
            }
            return new Activities(); // return an empty object if we didn't successfully download/deserialize
        }

        #endregion

        #region public methods

        public ActiveApiGateway()
        {
            _request = new ActiveApiRequest();
        }

        public ActiveApiGateway(ActiveApiRequest request)
        {
            _request = request;
        }

        public IEnumerable<Activity> GetAll()
        {
            var activities = GetActivitiesFromActive();
            return activities.Results;
        }
        #endregion
    }
}

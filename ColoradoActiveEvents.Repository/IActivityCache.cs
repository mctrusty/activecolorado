using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ColoradoActiveEvents.Model;

namespace ColoradoActiveEvents.Repository
{
    public interface IActivityCache : IDisposable
    {
        void UpdateFromActive(object data);
        ReadOnlyCollection<Activity> GetCachedActivities();
        void SetWait();
        string LastUpdate();
    }
}
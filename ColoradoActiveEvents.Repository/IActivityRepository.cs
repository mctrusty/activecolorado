﻿using System;
using System.Collections.ObjectModel;
using ColoradoActiveEvents.Model;
using System.Collections.Generic;

[assembly: CLSCompliant(true)]
namespace ColoradoActiveEvents.Repository
{
    public interface IActivityRepository
    {
        Activity GetActivity(string eventId);
        ReadOnlyCollection<Activity> GetActivities(int skip, int take);
        ReadOnlyCollection<Activity> GetAllActivities();
        bool PopulateFromActiveApi(bool purgeFirst);

    }
}

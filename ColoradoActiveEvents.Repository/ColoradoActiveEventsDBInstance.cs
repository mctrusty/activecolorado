﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColoradoActiveEvents.Model;
using Wintellect.Sterling.Database;

namespace ColoradoActiveEvents.Repository
{
    public class ColoradoActiveEventsDBInstance : BaseDatabaseInstance
    {
        public override string Name
        {
            get { return "Colorado Active Events"; }
        }
        protected override List<ITableDefinition> RegisterTables()
        {
                return new List<ITableDefinition>
                {
                    CreateTableDefinition<Activity, Guid>(c => c.Id)
                };
        }
    }
}

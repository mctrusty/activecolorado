﻿using System;
using Wintellect.Sterling;

namespace ColoradoActiveEvents.Repository
{
    public interface ISterlingService : IDisposable
    {
        ISterlingDatabaseInstance Database { get; }
        void StartService();
        void StopService();
        void Exiting();
        void Exited();
    }
}
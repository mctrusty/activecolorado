﻿using System.Collections.Generic;
using ColoradoActiveEvents.Model;

namespace ColoradoActiveEvents.Repository
{
    /// <summary>
    /// Interface for the Active.com API Activity Search
    /// </summary>
    public interface IActiveApiGateway
    {
        IEnumerable<Activity> GetAll();
    }
}

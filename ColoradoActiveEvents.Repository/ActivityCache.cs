﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using ColoradoActiveEvents.Model;

namespace ColoradoActiveEvents.Repository
{
    /// <summary>
    /// Periodically check Active.com API and load the results into a local list
    /// </summary>
    public class ActivityCache: IActivityCache
    {
        private static IActiveApiGateway _apiGateway;
        private static Timer _timer;
        private static AutoResetEvent _auto = new AutoResetEvent(false);
        
        static List<Activity> _activities = new List<Activity>();
        private static DateTime _lastUpdate = DateTime.Now;

        public ActivityCache(IActiveApiGateway gateway)
        {
            _apiGateway = gateway;
            Start();
        }

        public void UpdateFromActive(object data)
        {
            // Download results and load into activity cache
            try
            {
                var activities = _apiGateway.GetAll().ToList(); //Time consuming, so we run it before locking
                if (activities.Any())
                {
                    lock (_activities)
                    {
                        _activities = activities;
                        _lastUpdate = DateTime.Now;
                    }
                }

                _auto.Set();
            }
            catch (Exception e)
            {
                Trace.WriteLine("Cache not filled: " + e.Message);
                _auto.Set();
                //TODO more detailed error handling 
            }
        }

        /// <summary>
        /// Queries (Polls) the ApiGateway on startup and then every minute
        /// </summary>
        private void Start()
        {
            _timer = new Timer(UpdateFromActive, "Updating", TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(1));
        }

        public void SetWait()
        {
            _auto.WaitOne(TimeSpan.FromSeconds(5));
        }

        public string LastUpdate()
        {
           lock (_activities) return _lastUpdate.ToShortTimeString();
        }

        public ReadOnlyCollection<Activity> GetCachedActivities()
        {
            _auto.Set();
            lock (_activities) return new ReadOnlyCollection<Activity>(_activities);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                if (_timer != null)
                    _timer.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}

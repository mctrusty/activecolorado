﻿using System;
using System.Diagnostics;
using Wintellect.Sterling;
using Wintellect.Sterling.Server.FileSystem;

namespace ColoradoActiveEvents.Repository
{
    /// <summary>
    /// Start/Stop the embedded instance of the sterling db service.
    /// </summary>
    public sealed class SterlingFileSystemDBService: ISterlingService
    {
        private SterlingEngine _engine;
        public static SterlingFileSystemDBService Current { get; private set; }
        public ISterlingDatabaseInstance Database { get; private set; }
        private SterlingDefaultLogger _logger;

        public void StartService()
        {
            _engine = new SterlingEngine();
            Current = this;
            _engine.Activate();
            
            //TODO check for change in metadata due to change in underlying model/model dll.
            Database = _engine.SterlingDatabase.RegisterDatabase<ColoradoActiveEventsDBInstance>(new FileSystemDriver());
        }
        
        public void StopService()
        { return; }
        
        public void Exiting()
        {
            if (Debugger.IsAttached && _logger != null)
            {
                _logger.Detach();
            }
        }

        public void Exited()
        {
            Dispose();
            _engine = null;
            return;
        }

        public void Dispose()
        {
            if (_engine != null)
            {
                _engine.Dispose();
            }
            GC.SuppressFinalize(this);
        }
    } 
}

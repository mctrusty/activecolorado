﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using ColoradoActiveEvents.Model;
using Wintellect.Sterling;

namespace ColoradoActiveEvents.Repository
{
    /// <summary>
    /// Repository for Sterling NoSQL OODB
    /// </summary>
    public class ActivityRepository : IActivityRepository
    {
        private readonly IActiveApiGateway _activeApiGateway;
        private readonly ISterlingDatabaseInstance _databaseInstance;
        private readonly IActivityCache _activityCache;

        public ActivityRepository(IActiveApiGateway apiGateway, ISterlingDatabaseInstance sterlingDBInstance, IActivityCache activityCache)
        {
            _activeApiGateway = apiGateway;
            _databaseInstance = sterlingDBInstance;
            _activityCache = activityCache;
        }

        /// <summary>
        /// Select All Activities
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Activity> GetAllActivities()
        {
            var recs = (from o in _databaseInstance.Query<Activity, Guid>()
                        select o.LazyValue.Value).ToList();
            return new ReadOnlyCollection<Activity>(recs);
        }

        /// <summary>
        /// Get a number of Activity entries later in the repository
        /// </summary>
        /// <param name="skip">skip the first n entries</param>
        /// <param name="take">take m entries</param>
        /// <returns></returns>
        public ReadOnlyCollection<Activity> GetActivities(int skip, int take)
        {
            var recs = (from o in _databaseInstance.Query<Activity, Guid>()
                        select o.LazyValue.Value).Skip(skip).Take(take);
            return new ReadOnlyCollection<Activity>(recs.ToList());
        }

        /// <summary>
        /// Get an entry based on the Active.com eventId
        /// Ideally, I should implement a Custom Serializer for the eventId. Because the 
        /// eventId is in the meta array for an active.com result, I need to implement a 
        /// custom serializer in order to use eventId for a Key or Index in Sterling.
        /// </summary>
        /// <param name="eventId">Active.com eventId</param>
        /// <returns>Entry that matches the active.com eventId</returns>
        public Activity GetActivity(string eventId)
        {
            var activity = (from o in _databaseInstance.Query<Activity, Guid>()
                            select o.LazyValue.Value).Where(a => a.MetaInfo.EventId == eventId);
            return activity.FirstOrDefault();
        }

        /// <summary>
        /// Add results from Active.com API to our DB
        /// </summary>
        /// <param name="purgeFirst">Drop all current entries first</param>
        public bool PopulateFromActiveApi(bool purgeFirst)
        {
            Debug.Assert(_databaseInstance != null, "_databaseInstance != null");

            try
            {
                // check local cache
                _activityCache.SetWait(); // Set event to wait for cache 
                var activities = _activityCache.GetCachedActivities();

                // if cache hasn't been populated, we try to fill the db directly from Active.com gateway
                // This is paranoid and probably unnecessary but I'm leaving it in for now.
                if (activities == null || activities.Count == 0)
                {
                    Console.WriteLine("No activities in cache. Calling Active.com (takes a few seconds).");
                    var fetch = _activeApiGateway.GetAll();
                    if (fetch != null)
                        activities = new ReadOnlyCollection<Activity>(fetch.ToList());
                }


                // if our activity fetch worked, re-populate the local db.
                if (activities.Count > 0)
                {
                    if (purgeFirst)
                        _databaseInstance.Purge();
                    foreach (Activity activity in activities)
                    {
                        activity.Id = Guid.NewGuid(); //activity.MetaInfo.EventId;
                        _databaseInstance.Save(activity);
                    }

                    _databaseInstance.Flush();
                }
                else
                {
                    Console.WriteLine("No activities found from Active.com. Local storage not refreshed.");
                    Console.ReadKey();
                }
            }
            catch (NullReferenceException nullex)
            {
                Trace.WriteLine(nullex);
                return false;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return false;
            }

            return true;
        }


    }
}

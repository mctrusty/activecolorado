﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ColoradoActiveEvents.Model;
using ColoradoActiveEvents.Repository;

[assembly: CLSCompliant(true)]
namespace ColoradoActiveEvents.Services
{
    /// <summary>
    /// Handles the Database setup, context, and calls for the Main program
    /// </summary>
    public class MainController : IDisposable
    {
        private ISterlingService _dbService; 
        private IActiveApiGateway _activeApiGateway;
        private IActivityRepository _repository;
        private ActivityCache _apiCache;

        /// <summary>
        /// Default constructor sets up the regular dependencies.
        /// </summary>
        public MainController()
        {
            _activeApiGateway = new ActiveApiGateway();
            _apiCache = new ActivityCache(_activeApiGateway);

            _dbService = new SterlingFileSystemDBService();
            _dbService.StartService();
            
            _repository = new ActivityRepository(_activeApiGateway, _dbService.Database, _apiCache);
            
        }

        /// <summary>
        /// Dependency injected constructor.
        /// </summary>
        /// <param name="dbService">Database service</param>
        /// <param name="activeApiGateway">API Gateway</param>
        /// <param name="repository">Activity Repository</param>
        public MainController(ISterlingService dbService, IActiveApiGateway activeApiGateway,
                              IActivityRepository repository)
        {
            _activeApiGateway = activeApiGateway;
            _apiCache = new ActivityCache(_activeApiGateway);

            _dbService = dbService;
            _dbService.StartService();
            
            _repository = repository;
        }

        public string GetLastUpdate()
        {
            return _apiCache.LastUpdate();
        }

        public ReadOnlyCollection<Activity> GetActivities(int skip, int take)
        {
            return _repository.GetActivities(skip, take);
        }

        public Activity GetActivityDetails(string eventId)
        {
            return _repository.GetActivity(eventId);
        }

        /// <summary>
        /// Refresh the local data store with Active.com events
        /// </summary>
        public void RefreshCache()
        {
            _repository.PopulateFromActiveApi(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                if (_dbService != null)
                {
                    _dbService.StopService();
                    _dbService.Dispose();
                }
            }

            _apiCache.Dispose();
            _activeApiGateway = null;
            _repository = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

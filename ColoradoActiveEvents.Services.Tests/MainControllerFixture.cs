﻿using System;
using NUnit.Framework;
using Moq;
using ColoradoActiveEvents.Repository;

namespace ColoradoActiveEvents.Services.Tests
{

    [TestFixture]
    public class MainControllerFixture
    {
        private Mock<ISterlingService> _service;
        private Mock<IActiveApiGateway> _gateway;
        private Mock<IActivityRepository> _repository;
        private MainController _controller;

        [SetUp]
        public void Setup()
        {
            _service = new Mock<ISterlingService>();
            _gateway = new Mock<IActiveApiGateway>();
            _repository = new Mock<IActivityRepository>();
            _controller = new MainController(_service.Object, _gateway.Object, _repository.Object);
        }

        [Test]
        public void GetActivities_calls_repo_once()
        {
            _controller.GetActivities(0, 10);
            _repository.Verify(r => r.GetActivities(0,10), Times.Exactly(1));
        }

        [Test]
        public void GetActivityDetails_calls_repo_once()
        {
            var activityDetails = _controller.GetActivityDetails("1");
            _repository.Verify(r => r.GetActivity("1"), Times.Exactly(1));
        }

        [Test]
        public void RefreshCache_hits_active_api()
        {
            _controller.RefreshCache();
            _repository.Verify(r => r.PopulateFromActiveApi(true), Times.Exactly(1));
        }

        [Test]
        public void Dispose_destroys_repository()
        {
            var controller = new MainController(_service.Object, _gateway.Object, _repository.Object);
            controller.Dispose();
            // Should get a null reference exception because the repository gets nulled on Dispose
            Assert.Throws<NullReferenceException>(() => controller.GetActivities(0, 10));
        }

        [Test]
        public void Dispose_nulls_gateway()
        {
            var controller = new MainController(_service.Object, _gateway.Object, _repository.Object);
            controller.Dispose();
            // Should get a null reference exception because gateway gets nulled on dispose
            Assert.Throws<NullReferenceException>(() => controller.RefreshCache());
        }

        [TearDown]
        public void TearDown()
        {
            

        }
    }
}

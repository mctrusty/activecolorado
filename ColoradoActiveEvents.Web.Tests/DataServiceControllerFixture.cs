﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using ColoradoActiveEvents.Model;
using ColoradoActiveEvents.Repository;
using ColoradoActiveEvents.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace ColoradoActiveEvents.Web.Tests
{
    [TestFixture]
    public class DataServiceControllerFixture
    {
        private Mock<ISterlingService> _service;
        private Mock<IActiveApiGateway> _gateway;
        private Mock<IActivityRepository> _repository;
        private DataServiceController _controller;

        [SetUp]
        public void Setup()
        {
            _service = new Mock<ISterlingService>();
            _gateway = new Mock<IActiveApiGateway>();
            _repository = new Mock<IActivityRepository>();
            _controller = new DataServiceController(_gateway.Object, _service.Object, _repository.Object);
        }

        [Test]
        public void DataServiceController_GetReturnsCorrectNumber()
        {
            var activityList = new List<Activity>();
            for (var i = 0; i < 100; i++)
            {
                activityList.Add(new Activity() {EventId = i.ToString()});
            }
            var activities = new ReadOnlyCollection<Activity>(activityList);
            _repository.Setup(r => r.GetAllActivities()).Returns(activities);
            Assert.AreEqual(100, _controller.Get().Count);
        }

        [Test]
        public void GetActivities_calls_repo_once()
        {
            _controller.Get();
            _repository.Verify(r => r.GetAllActivities(), Times.Exactly(1));
        }

        [Test]
        public void RefreshCache_hits_active_api()
        {
            _controller.Refresh();
            _repository.Verify(r => r.PopulateFromActiveApi(true), Times.Exactly(1));
        }
    }
}

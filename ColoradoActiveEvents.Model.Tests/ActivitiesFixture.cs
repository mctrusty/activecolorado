﻿using Moq;
using NUnit.Framework;
using ColoradoActiveEvents.Repository;

namespace ColoradoActiveEvents.Model.Tests
{
    [TestFixture]
    public class ActivitiesFixture
    {
        /// <summary>
        /// What happens when no json is returned from active.com API?
        /// </summary>
        [Test]
        public void ActivitesIsEmpty()
        {
            var gateway = new Mock<IActiveApiGateway>();
            var repo = new Mock<IActivityRepository>();
            repo.Setup(r => r.PopulateFromActiveApi(true)).Verifiable();
        }
    }
}

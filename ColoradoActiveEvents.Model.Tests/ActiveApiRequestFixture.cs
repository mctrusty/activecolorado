﻿using System;
using NUnit.Framework;

namespace ColoradoActiveEvents.Model.Tests
{
    [TestFixture]
    public class ActiveApiRequestFixture
    {
        [Test]
        public void ActiveApiRequest_builds_default_string()
        {
            var query = new ActiveApiRequest();
            const string expectedBase = "http://api.amp.active.com/search/";
            const string expectedQueryString = "?v=json&l=CO&r=5&meta:startDate:daterange:today.. meta:channel=Running&f=activities&s=relevance&num=100&page=1&api_key=6dpss5ksng6ecp7bkg9eu4pr";
            
            Assert.AreEqual(expectedBase, query.ApiBase.ToString());
            Assert.AreEqual(expectedQueryString, query.QueryString);
        }

        [Test]
        public void ActiveApiRequest_builds_correct_strings()
        {
            var query = new ActiveApiRequest()
                {
                    ApiBase = new Uri("http://test/"),
                    Format = "v=json",
                    Location = "l=CO",
                    Radius = "r=20",
                    //Leave Meta out to see if constructor fills it in correctly
                    Sort = "s=relevance",
                    Number = "num=50",
                    Page = "page=2",
                    ApiKey = "api_key=XXX"

                };
            
            const string expectedBase = "http://test/";
            const string expectedQueryString = "?v=json&l=CO&r=20&meta:startDate:daterange:today.. meta:channel=Running&f=activities&s=relevance&num=50&page=2&api_key=XXX";
            
            Assert.AreEqual(expectedBase, query.ApiBase.ToString());
            Assert.AreEqual(expectedQueryString, query.QueryString);
        }
    }
}

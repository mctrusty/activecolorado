﻿define(function (require) {
    var logger = require('services/logger');
    return {
        getActivities: function () {
            // make json call to our api
            return $.ajax(
            {
                type: "GET",
                url: "/api/DataService",
                dataType: "json"
            }).done(function (data) {
                return data;
            });
        },
        refresh: function () {
            return $.ajax(
                {
                    type: "POST",
                    url: "api/DataService/Refresh",
                    dataType: "json"
                }).done(function (result) {
                    logger.log('Activities Refreshed', null, 'activitiesRepo', false);
                    return result;
                });
        }
    };
});
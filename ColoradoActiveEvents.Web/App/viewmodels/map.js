﻿define(function (require) {
    var app = require('durandal/app');
    var geoPosition = require('spatial/geoPosition');

    var geoSuccess = function (p) {
        app.showMessage("found you at latitude: " + p.coords.latitude + ", longitude: " + p.coords.longitude);
    };

    var geoError = function (p) {
        alert("Unable to geolocate");
    };

    function showMap(loc) {
        $("#geo-wrapper").css({ 'width': '320px', 'height': '350px' });
        var map = new google.maps.Map(document.getElementById("geo-wrapper"), { zoom: 14, mapTypeControl: true, zoomControl: true, mapTypeId: google.maps.MapTypeId.ROADMAP });
        var center = new google.maps.LatLng(loc.coords.latitude, loc.coords.longitude);
        map.setCenter(center);
        var marker = new google.maps.Marker({ map: map, position: center, draggable: false, title: "You are here (more or less)" });
    }
    
    function showMapError() {
        $("#live-geolocation").html('Unable to determine your location.');
    }

    return {
        displayName: 'Map',
        showMap: function () {
            if (geoPosition.init()) {
                geoPosition.getCurrentPosition(geoSuccess, geoError);
            }
        }
    };
});
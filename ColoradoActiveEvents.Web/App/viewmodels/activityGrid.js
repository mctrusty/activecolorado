﻿define(function (require) {
    var app = require('durandal/app');
    
    var ActivityGrid = function (configuration) {
        var self = this;
        self.data = configuration.data;
        self.currentPageIndex = ko.observable(0);
        self.pageSize = configuration.pageSize || 5;

        // If you don't specify columns configuration, we'll use scaffolding
        self.columns = configuration.columns || getColumnsForScaffolding(ko.utils.unwrapObservable(this.data));

        self.itemsOnCurrentPage = ko.computed(function () {
            var startIndex = self.pageSize * self.currentPageIndex();
            return self.data.slice(startIndex, startIndex + self.pageSize);
        }, this);

        self.maxPageIndex = ko.computed(function () {
            return Math.ceil(ko.utils.unwrapObservable(self.data).length / self.pageSize) - 1;
        }, this);

        self.select = function (item) {
            //the app model allows easy display of modal dialogs by passing a view model
            //views are usually located by convention, but you an specify it as well with viewUrl
            item.viewUrl = 'views/activityDetails';
            app.showModal(item);
            //app.showMessage("Selected");
        };
    };

    ActivityGrid.prototype.getColumnsForScaffolding = function (data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ headerText: propertyName, rowText: propertyName });
        }
        return columns;
    };

    return ActivityGrid;
});
﻿define(function (require) {
    var app = require('durandal/app');
    var activitiesRepo = require('repositories/activitiesRepository');

    var activities = ko.observableArray([]);
    function activate() {
        return activitiesRepo.getActivities().then(function (results) {
            activities(results);
        });
    };

    var vm = {
        displayName: 'Upcoming Colorado Events',
        showMessage: function () {
            app.showMessage("TODO: Refresh from Active");
        },
        activities: activities,
        refresh: activitiesRepo.refresh,
        activate: activate,
    };

    return vm;
    
});
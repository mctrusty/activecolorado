﻿define([], function (require) {
    var ActivityGrid = require('./activityGrid');
    var events = require('./events');
    var logger = require('services/logger');
    
    var gridViewModel = new ActivityGrid({
        data: events.activities, //activities is an observableArray.
        columns: [
            { headerText: "Event", rowText: "title" },
            { headerText: "date", rowText: function (item) { return item.meta.startDate; }},
            { headerText: "details", rowText: function (item) { return "<a href=#>details</a>"; }}
        ],
        pageSize: 10
    });

    var sortByName = function () {
        events.activities.sort(function (a, b) {
            return a.title < b.title ? -1 : 1;
        });
    };

    var jumpToFirstPage = function () {
        gridViewModel.currentPageIndex(0);
    };

    var refreshEvents = function() {
        // refresh returns a promise. Use that to relay success/failure of refresh
        // also need to update events.activities...
        events.refresh().done(function(result) {
            logger.log('Events Refreshed', null, 'activeEvents', true);
            events.activate();
            //update items...
        });
    };

    return {
        items: events.activities,
        sortByName: sortByName,
        jumpToFirstPage: jumpToFirstPage,
        refreshEvents: refreshEvents,
        gridViewModel: gridViewModel,
        activate: events.activate,
    };
});
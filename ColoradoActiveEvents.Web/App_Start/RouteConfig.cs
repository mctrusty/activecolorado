using System.Web.Mvc;
using System.Web.Routing;

namespace ColoradoActiveEvents.Web.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ColoradoActive",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "ColoradoActive", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
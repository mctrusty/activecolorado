﻿using ColoradoActiveEvents.Web;
using ColoradoActiveEvents.Repository;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using ColoradoActiveEvents.Web.App_Start;

namespace ColoradoActiveEvents.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly SterlingFileSystemDBService _dbService = new SterlingFileSystemDBService();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            _dbService.StartService();
        }

        protected void Application_End()
        {
            _dbService.StopService();
            _dbService.Dispose();
        }
    }
}
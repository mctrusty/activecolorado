﻿using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ColoradoActiveEvents.Model;
using ColoradoActiveEvents.Repository;

namespace ColoradoActiveEvents.Web.Controllers
{
    public class DataServiceController : ApiController
    {
        //TODO - DI
        private static IActiveApiGateway _activeApiGateway;
        private static ISterlingService _currentDb;
        private static IActivityCache _activityCache;
        private static IActivityRepository _activityRepository;

        public DataServiceController()
        {
            _activeApiGateway = new ActiveApiGateway();
            _currentDb = SterlingFileSystemDBService.Current;
            _activityCache = new ActivityCache(_activeApiGateway);
            _activityRepository = new ActivityRepository(_activeApiGateway, _currentDb.Database, _activityCache);
        }

        //DI constructor, mostly for unit testing
        public DataServiceController(IActiveApiGateway gateway, ISterlingService currentDb, IActivityRepository repo)
        {
            _activeApiGateway = gateway;
            _currentDb = currentDb;
            _activityCache = new ActivityCache(_activeApiGateway);
            _activityRepository = repo;
        }

        // GET api/<controller>
        public ReadOnlyCollection<Activity> Get()
        {
            var repoacts = _activityRepository.GetAllActivities();
            return repoacts;
        }

        // POST api/DataService/Refresh
        [HttpPost]
        public HttpResponseMessage Refresh()
        {
            HttpResponseMessage response;

            if (_activityRepository.PopulateFromActiveApi(true))
            {
                response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"success\":\"true\"}]")
                    };
            }
            else
            {
                response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"success\":\"true\"}]")
                    };
            }

            return response;   
        }
    }
}
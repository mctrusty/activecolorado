﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using ColoradoActiveEvents.Model;
using ColoradoActiveEvents.Repository;
using ColoradoActiveEvents.Services;

namespace ColoradoActiveEvents
{
    class Program
    {
        private const string ColoradoActiveEventsTitle = "+++   Colorado Active.com Events   +++";
        private static MainController _mainController;

        // _loaded flag handles an issue with skip/take being an exact multiple of the number of 
        // results returned from Active.com. On, e.g. 100 results with a take value of 10, the 
        // !activities.Any() will end up true after the user pages through the entire list
        private static bool _loaded = false;

        /// <summary>
        /// Small Console Application to intervace with ColoradoEvents db application.
        /// Menu template comes from http://www.daniweb.com/software-development/csharp/code/371819/code-template-for-a-menu-in-a-console-application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            _mainController = new MainController();
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));

            // menu vars
            const int maxMenuItems = 4;
            int selector = 0;
            bool good = false;

            //paging vars
            int skip = 0;
            const int take = 10;

            //menu loop
            while (selector != maxMenuItems)
            {
                Console.Clear();
                DrawTitle();

                var activities = _mainController.GetActivities(skip,take);

                //if there are no cached results, we need to populate our local store
                //note: Any() doesn't force evaluation
                if (!activities.Any() && !_loaded)
                {
                    Console.WriteLine("Empty Database. Please wait a moment for it to populate from Active API");
                    _mainController.RefreshCache();
                    activities = _mainController.GetActivities(skip, 10);
                }
                _loaded = true;

                DrawPage(activities);
                
                DrawMenu(maxMenuItems);
                
                good = int.TryParse(Console.ReadLine(), out selector);
                if (good)
                {
                    switch (selector)
                    {
                        case 1:
                            if (activities.Count() == take)
                                skip += take;
                            else
                                skip = 0;
                            break;
                        case 2:
                            Console.WriteLine("Refreshing DB, Please Wait a Moment");
                            _mainController.RefreshCache();
                            skip = 0;
                            break;
                        case 3:
                            Console.Write("Enter Event ID for more details: ");
                            var eventId = Console.ReadLine();
                            var details = _mainController.GetActivityDetails(eventId);
                            OutputDetails(details);
                            Console.ReadKey();
                            break;
                        default:
                            if (selector != maxMenuItems)
                            {
                                ErrorMessage();
                            }
                            break;
                    }
                }
                else
                {
                    ErrorMessage();
                }
            }
        }

        private static void OutputDetails(Activity details)
        {
            if (details != null)
            {
                Console.WriteLine(details.MetaInfo.Summary != String.Empty
                                      ? details.MetaInfo.Summary
                                      : "No Summary found for that ID.");
            }
            else
                Console.WriteLine("No Summary found for that ID.");
        }

        private static void DrawPage(IEnumerable<Activity> activities)
        {
            foreach (Activity result in activities)
            {
                Console.WriteLine("[{0}]\t- {1}", result.MetaInfo.EventId, result.Title);
            }
        }

        private static void ErrorMessage()
        {
            Console.WriteLine("Typing error, press key to continue.");
        }
        private static void DrawStarLine()
        {
            Console.WriteLine("".PadRight(ColoradoActiveEventsTitle.Length, '*'));
        }
        private static void DrawTitle()
        {
            DrawStarLine();
            Console.WriteLine(ColoradoActiveEventsTitle);
            DrawStarLine();
        }
        private static void DrawMenu(int maxitems)
        {
            DrawStarLine();
            Console.WriteLine(" 1. Next Page");
            Console.WriteLine(" 2. Refresh From Active");
            Console.WriteLine(" 3. Get Details for Entry");
            Console.WriteLine(" 4. Exit");
            DrawStarLine();
            Console.WriteLine("Make your choice: type 1, 2, 3, or {0} for exit", maxitems);
            DrawStarLine();
        }
    }
}

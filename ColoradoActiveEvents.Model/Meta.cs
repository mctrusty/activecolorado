﻿using System;
using Newtonsoft.Json;

[assembly: CLSCompliant(true)]
namespace ColoradoActiveEvents.Model
{
    public class Meta
    {
        [JsonProperty("eventId")]
        public string EventId { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("startDate")]
        public string StartDate { get; set; }
    }
}

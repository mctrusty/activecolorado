﻿using System;
using System.Text;

namespace ColoradoActiveEvents.Model
{
    public class ActiveApiRequest
    {
        public Uri ApiBase { get; set; }
        public string Format { get; set; }
        public string Radius { get; set; }
        public string Location { get; set; }
        public string Meta { get; set; }
        public string Facet { get; set; }
        public string Sort { get; set; }
        public string Number { get; set; }
        public string Page { get; set; }
        public string ApiKey { get; set; }

        public string QueryString
        {
            get
            {
                const char amp = '&';
                var queryString = new StringBuilder("?");
                queryString.Append(Format).Append(amp);
                queryString.Append(Location).Append(amp);
                queryString.Append(Radius).Append(amp);
                queryString.Append(Meta).Append(amp);
                queryString.Append(Facet).Append(amp);
                queryString.Append(Sort).Append(amp);
                queryString.Append(Number).Append(amp);
                queryString.Append(Page).Append(amp);
                queryString.Append(ApiKey);

                return queryString.ToString();

            }
        }

        public ActiveApiRequest()
        {
            ApiBase = new Uri("http://api.amp.active.com/search/");
            Format = "v=json";
            Location = "l=CO";
            Radius = "r=5";
            Meta = "meta:startDate:daterange:today.. meta:channel=Running";
            Facet = "f=activities";
            Sort = "s=relevance";
            Number = "num=100";
            Page = "page=1";
            ApiKey = "api_key=6dpss5ksng6ecp7bkg9eu4pr";
        }
    }
}

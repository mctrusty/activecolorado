﻿using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace ColoradoActiveEvents.Model
{
    public class Activities
    {
        [JsonProperty("endIndex")]
        public int EndIndex { get; set; }

        [JsonProperty("numberOfResults")]
        public int NumberOfResults { get; set; }

        [JsonProperty("pageSize")]
        public int PageSize { get; set; }

        [JsonProperty("searchTime")]
        public int SearchTime { get; set; }

        [JsonProperty("_results")]
        public Collection<Activity> Results { get; set; }

    }
}

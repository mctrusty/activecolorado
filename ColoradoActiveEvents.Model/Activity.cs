﻿using System;
using Newtonsoft.Json;

namespace ColoradoActiveEvents.Model
{
    /// <summary>
    /// Model class for Colorado Events returned from Active.com API
    /// </summary>
    public class Activity
    {
        public  Guid Id { get; set; }

        public string EventId { get; set; }

        [JsonProperty("escapedUrl")]
        public string EscapedUrl { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("meta")]
        public Meta MetaInfo { get; set; }

    }
}
